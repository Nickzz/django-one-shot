from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def my_list(request):
    todolist = TodoList.objects.all()
    context = {"my_list_object": todolist}
    return render(request, "todos/mylist.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {"todo_list_object": todolist}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
        context = {"form": form}
        return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)

    # To add something to the model, like setting a user,
    # use something like this:
    #
    # model_instance = form.save(commit=False)
    # model_instance.user = request.user
    # model_instance.save()
    # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm(instance=todolist)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
        context = {"form": form}
        return render(request, "todos/createitem.html", context)


def update_todo_item(request, id):
    todoitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.id)

    # To add something to the model, like setting a user,
    # use something like this:
    #
    # model_instance = form.save(commit=False)
    # model_instance.user = request.user
    # model_instance.save()
    # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoItemForm(instance=todoitem)

    context = {"form": form}

    return render(request, "todos/edit.html", context)

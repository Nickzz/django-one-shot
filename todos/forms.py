from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoListForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = TodoList  # Step 3
        fields = [  # Step 4
            "name",
        ]


class TodoItemForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = TodoItem  # Step 3
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
